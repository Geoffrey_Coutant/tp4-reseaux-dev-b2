# TP4 : I'm Socketing, r u soketin ?

# I. Simple bs program

## 1. First steps

### 🌞 bs_server_I1.py

```
% python bs_server_I1.py
Connected by ('10.1.2.11', 46748)
Données reçues du client : b'salut'
```

### 🌞 bs_client_I1.py

```
$ python bs_client_I1.py
Le serveur a répondu b'Hi mate !'
```

### ➜ Pour quitter proprement

```
sys.exit(0)
```

### 🌞 Commandes...

```
$ git clone https://gitlab.com/Geoffrey_Coutant/tp4-reseaux-dev-b2.git
[...]
$ sudo firewall-cmd --permanent --zone=public --add-port=8888/tcp
success
```

## 2. User friendly

### 🌞 bs_client_I2.py

```
$ python bs_client_I2.py
Connecté avec succès au serveur 10.1.2.10 sur le port 8888
Que veut-tu envoyer au serveur :waf
Le serveur a répondu b'ptdr t ki'
```
```
$ python bs_client_I2.py
Connecté avec succès au serveur 10.1.2.10 sur le port 8888
Que veut-tu envoyer au serveur :meo
Le serveur a répondu b'Meo \xc3\xa0 toi confr\xc3\xa8re.'
```
```
$ python bs_client_I2.py
Connecté avec succès au serveur 10.1.2.10 sur le port 8888
Que veut-tu envoyer au serveur :salut
Le serveur a répondu b'Mes respects humble humain.'
```

### 🌞 bs_server_I2.py

```
$ python bs_server_I2.py
Un client vient de se co et son IP c'est ('10.1.2.11', 47558)
Données reçues du client : b'waf'
```
```
$ python bs_server_I2.py
Un client vient de se co et son IP c'est ('10.1.2.11', 49654)
Données reçues du client : b'meo'
```
```
$ python bs_server_I2.py
Un client vient de se co et son IP c'est ('10.1.2.11', 50594)
Données reçues du client : b'salut'
```

## 3. You say client I hear control

### 🌞 bs_client_I3.py

```
$ python bs_client_I3.py
Connecté avec succès au serveur 10.1.2.10 sur le port 8888
Que veut-tu envoyer au serveur :salut
Traceback (most recent call last):
  File "/home/rocky/tp4-reseaux-dev-b2/bs_client_I1.py", line 28, in <module>
    raise Exception('invalid input: should be \'meo\' or \'waf\'')
Exception: invalid input: should be 'meo' or 'waf'
```
```
$ python bs_client_I3.py
Connecté avec succès au serveur 10.1.2.10 sur le port 8888
Que veut-tu envoyer au serveur :waf
Le serveur a répondu b'ptdr t ki'
```

```
$ python bs_client_I3.py
Connecté avec succès au serveur 10.1.2.10 sur le port 8888
Que veut-tu envoyer au serveur :meo
Le serveur a répondu b'Meo \xc3\xa0 toi confr\xc3\xa8re.'
```

```
$ python bs_client_I3.py
Connecté avec succès au serveur 10.1.2.10 sur le port 8888
Que veut-tu envoyer au serveur :3
Traceback (most recent call last):
  File "/home/rocky/tp4-reseaux-dev-b2/bs_client_I3.py", line 28, in <module>
    raise Exception('invalid input: should be \'meo\' or \'waf\'')
Exception: invalid input: should be 'meo' or 'waf'
```

## II. You say dev I say good practices

## 1. Args

### 🌞 bs_server_II1.py

```
$ python bs_server_II1.py -p 4
ERROR Le port spécifié est un port privilégié. Spécifiez un port au dessus de 1024.

$ python bs_server_II1.py -g
usage: bs_server_II1.py [-h] [-p PORT]
bs_server_II1.py: error: unrecognized arguments: -g

$ python bs_server_II1.py -h
usage: bs_server_II1.py [-h] [-p PORT]
optional arguments:
  -h, --help            show this help message and exit
  -p PORT, --port PORT
```

## 2. Logs



## A. Logs serveur
