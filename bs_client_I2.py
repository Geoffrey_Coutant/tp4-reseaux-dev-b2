import socket
import sys

host = '10.1.2.10'
port = 8888
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
    s.connect((host, port))
except Exception as e:
    print(f"La connexion à foiré : {e}")
    exit(1)
print(f"Connecté avec succès au serveur {host} sur le port {port}")
serv_input = input('Que veut-tu envoyer au serveur :')
s.sendall(serv_input)
data = s.recv(1024)
s.close()