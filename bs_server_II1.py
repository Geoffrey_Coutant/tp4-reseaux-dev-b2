import socket
import sys
import argparse

parsers = argparse.ArgumentParser()
parsers.add_argument('-p', '--port', action='store')
arg = parsers.parse_args()
if arg.port:
    if int(arg.port) not in range(0, 65535):
        print(f"ERROR Le port spécifié n'est pas un port possible (de 0 à 65535)")
        exit(1)
    if int(arg.port) < 1024:
        print(f"ERROR Le port spécifié est un port privilégié. Spécifiez un port au dessus de 1024.")
        exit(1)
host = ''
port = 13337 if not arg.port else int(arg.port)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind((host, port))
sock.listen(1)
conn, addr = sock.accept()
print(f"Un client vient de se co et son IP c'est {addr}")
while True:
